import React, { useState } from "react";
import "./app.css";

const App = () => {
  const ws = new WebSocket(
    "wss://stream.binance.com:9443/ws/btcusdt@trade/bnbusdt@trade/ethusdt@trade/solusdt@trade"
  );

  const [btc, setBtc] = useState([]);
  const [bnb, setBnb] = useState([]);
  const [eth, setEth] = useState([]);
  const [sol, setSol] = useState([]);

  ws.onmessage = function (event) {
    const myData = JSON.parse(event.data.toString());
    if (myData.s === "BTCUSDT") {
      setBtc(myData);
    }
    if (myData.s === "BNBUSDT") {
      setBnb(myData);
    }
    if (myData.s === "ETHUSDT") {
      setEth(myData);
    }
    if (myData.s === "SOLUSDT") {
      setSol(myData);
    }
  };

  return (
    <div className="app">
          <span>{btc.e}</span>
          <span>{btc.E}</span>
          <span>{btc.s}</span>
          <span>{btc.t}</span>
          <span>{btc.p}</span>
          <span>{btc.q}</span>
          <span>{btc.b}</span>
          <span>{btc.a}</span>
          <span>{btc.T}</span>
          <span>{btc.m}</span>
          <span>{btc.M}</span>
          <br />
          <span>{bnb.e}</span>
          <span>{bnb.E}</span>
          <span>{bnb.s}</span>
          <span>{bnb.t}</span>
          <span>{bnb.p}</span>
          <span>{bnb.q}</span>
          <span>{bnb.b}</span>
          <span>{bnb.a}</span>
          <span>{bnb.T}</span>
          <span>{bnb.m}</span>
          <span>{bnb.M}</span>
          <br />
          <span>{eth.e}</span>
          <span>{eth.E}</span>
          <span>{eth.s}</span>
          <span>{eth.t}</span>
          <span>{eth.p}</span>
          <span>{eth.q}</span>
          <span>{eth.b}</span>
          <span>{eth.a}</span>
          <span>{eth.T}</span>
          <span>{eth.m}</span>
          <span>{eth.M}</span>
          <br />
          <span>{sol.e}</span>
          <span>{sol.E}</span>
          <span>{sol.s}</span>
          <span>{sol.t}</span>
          <span>{sol.p}</span>
          <span>{sol.q}</span>
          <span>{sol.b}</span>
          <span>{sol.a}</span>
          <span>{sol.T}</span>
          <span>{sol.m}</span>
          <span>{sol.M}</span>
          <br />
    </div>
  );
};

export default App;
